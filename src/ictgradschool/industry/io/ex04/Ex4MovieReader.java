package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        File myFile = new File(fileName);
        int i = 0;
        try(Scanner scanner1 = new Scanner(myFile)){
            int count = 0;
            while (scanner1.hasNextLine()) {
                count++;
                scanner1.nextLine();
            }
            Scanner scanner = new Scanner(myFile);
            scanner.useDelimiter(",|\\r\\n");
            Movie[] films = new Movie[count];
            while(scanner.hasNext()){
                String n = scanner.next().replaceAll("\"" , "");
                int y = scanner.nextInt();
                int m = scanner.nextInt();
                String d = scanner.next().replaceAll("\"" , "");
                films[i] = new Movie(n,y,m,d);
                i++;
            }
            return films;
        }
        catch(NullPointerException e){
            System.out.println("Error: "+e.getMessage());
        }
        catch(IOException e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args)
        {
        new Ex4MovieReader().start();
    }
}
