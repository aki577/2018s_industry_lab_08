package ictgradschool.industry.io;

import java.io.FileReader;
import java.io.IOException;

public class practise
    {
    private void fileReaderEx01()
        {
        int num = 0;
        FileReader fR = null;
        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println(num);
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            fR.close();
        } catch (IOException e) {
            System.out.println("IO problem");
        }
        }


    public static void main(String[] args)
        {
        new practise().fileReaderEx01();
        }

    }
