package ictgradschool.industry.io.ex03;

import ictgradschool.Keyboard;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by anhyd on 20/03/2017.
 */
public class MovieReader {

    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);

        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);

    }

    /**
     * Reads movies from a file.
     *
     * @param fileName
     * @return
     */
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this method
        try (FileInputStream myMovieFile = new FileInputStream(fileName)) {
            DataInputStream movie = new DataInputStream(myMovieFile);
            int num = movie.readInt();
            Movie[] films = new Movie[num];
            for (int i = 0; i < num; i++) {
                films[i] = new Movie(movie.readUTF(), movie.readInt(), movie.readInt(), movie.readUTF());
            }
            return films;
        } catch (IOException err) {
            System.out.println("Error: " + err.getMessage());
        }

        System.out.println("Movies loaded successfully from " + fileName + "!");
        return null;
    }

    //        int length = movie.readInt();
//        byte[] array = new byte[length];
//        movie.read(array);
//        String s = new String(array);
//        String s = "";
//        while(movie.available()>0){
//            Byte k = movie.readByte();
//            char c = (char)k.byteValue();
//            s += c;
//        }
//        System.out.println(s);
//        String[] str1 = s.split("\n");
//        String[] str2;
//        Movie[] films = new Movie[str1.length];
//        for (int i = 0; i < str1.length; i++) {
//            str2 = str1[i].split(",");
//            System.out.println(str2[0] + str2[1] + str2[2] + str2[3]);
//            films[i]= new Movie(str2[0], Integer.parseInt(str2[1]), Integer.parseInt(str2[2]), str2[3]);
//        }
//        movie.close();

    private void printMoviesArray(Movie[] films) {
        System.out.println("Movie Collection");
        System.out.println("================");
        // Step 2.  Complete the printMoviesArray() method
        for (int i = 0; i < films.length; i++) {
            System.out.println(films[i].toString());
        }
    }

    private Movie getMostRecentMovie(Movie[] films) {
        // Step 3.  Complete the getMostRecentMovie() method.
        Movie temp = null;
        for (int i = 1; i < films.length; i++) {
            if (films[i].isMoreRecentThan(films[i - 1])) {
                temp = films[i];
            }
        }
        return temp;
    }

    private Movie getLongestMovie(Movie[] films) {
        // Step 4.  Complete the getLongest() method.
        Movie temp = null;
        for (int i = 1; i < films.length; i++) {
            if (films[i].isLongerThan(films[i - 1])) {
                temp = films[i];
            }
        }
        return temp;
    }

    private void printResults(Movie mostRecent, Movie longest) {
        System.out.println();
        System.out.println("The most recent movie is: " + mostRecent.toString());
        System.out.println("The longest movie is: " + longest.toString());
    }

    private void printDirector(String movieName, Movie[] movies) {
        // Step 5. Complete the printDirector() method
        for (int i = 0; i < movies.length; i++) {
            if (movieName.equals(movies[i].getName())) {
                System.out.println(movieName + " was directed by " + movies[i].getDirector());
                return;
            }
        }
        System.out.println(movieName + " is not in the collection.");
    }

    public static void main(String[] args) {
        new MovieReader().start();
    }
}
