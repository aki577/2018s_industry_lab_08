package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class mainProgram
    {
    private player player1;
    private com computer;

    public mainProgram()
        {
        player1 = new player();
        computer = new com();
        }

    public void start() throws FileNotFoundException
        {

        System.out.println("----------------------------------");
        System.out.println("Welcome to Bulls and Cows game!");
        System.out.print("Enter your name: ");
        player1.setName(Keyboard.readInput());
        int[] a = loadFile();

        System.out.print("Enter the filename you want to put result in: ");
        String outputFilename = Keyboard.readInput();
        PrintStream out = new PrintStream(new FileOutputStream(outputFilename));
        PrintStream console = System.out;

        computer.setName("Computer");
        System.out.print("Randomly generating the secret code...");
        int secretCode = getSecretCode();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finished!");
        System.out.println("Don't forget that you only have seven attempts to guess the secret code!");
        System.out.println("Alright, the game begins.");
        System.out.println("----------------------------------");
        int count = 0;
        int i = a.length;
        while (true) {
            System.out.println("Do you want to use file number(1) or by yourself(2)?");
            System.out.print("Enter 1 or 2: ");
            int playerGuess = 0;
            int num = Integer.parseInt(Keyboard.readInput());
            switch (num) {
                case 1:
                    if (i < count) {
                        continue;
                    }
                    playerGuess = a[count];
                    break;
                case 2:
                    System.out.print("Your ");
                    switch (count) {
                        case 0:
                            System.out.print("first");
                            break;
                        case 1:
                            System.out.print("second");
                            break;
                        case 2:
                            System.out.print("third");
                            break;
                        case 3:
                            System.out.print("fourth");
                            break;
                        case 4:
                            System.out.print("fifth");
                            break;
                        case 5:
                            System.out.print("sixth");
                            break;
                        case 6:
                            System.out.print("last");
                            break;
                    }
                    System.out.print(" guess: ");
                    playerGuess = player1.guess();
                    break;
            }
            int computerGuess = computer.guess();

            System.setOut(out);
            printGuess(player1.getName(), playerGuess, count);
            printBullandCow(playerGuess, secretCode);
            printGuess(computer.getName(), computerGuess, count);
            printBullandCow(computerGuess, secretCode);
            boolean state1 = match(playerGuess, secretCode);
            boolean state2 = match(computerGuess, secretCode);
            if (state1 == true && state2 == true) {
                System.out.println("Both you and computer wins!");
                System.out.println("The Secret Code is: ");
                System.out.println(secretCode);
                break;
            } else if (state1 == true) {
                System.out.print("You Win!");
                System.out.println("The Secret Code is: ");
                System.out.println(secretCode);
                break;
            } else if (state2 == true) {
                System.out.println("Computer Win!");
                System.out.println("The Secret Code is: ");
                System.out.println(secretCode);
                break;
            }
            count++;
            if (count == 7) {
                System.out.println("No one wins!");
                System.out.println("The Secret Code is: " + secretCode);
                break;   //quit
            }
            System.setOut(console);
            System.out.println("----------------------------------");
        }
        System.out.println("Thanks for playing!");
        }

    public void printGuess(String name, int guess, int num)
        {
        System.out.print(name + "'s ");
        switch (num) {
            case 0:
                System.out.print("first");
                break;
            case 1:
                System.out.print("second");
                break;
            case 2:
                System.out.print("third");
                break;
            case 3:
                System.out.print("fourth");
                break;
            case 4:
                System.out.print("fifth");
                break;
            case 5:
                System.out.print("sixth");
                break;
            case 6:
                System.out.print("last");
                break;
        }
        System.out.println(" guess: " + guess);

        }

    public int getSecretCode()
        {
        int num = 0;
        for (int i = 0; i < 4; i++) {
            int random = (int) (Math.random() * 9 + 1);
            num += random * Math.pow(10, i);
        }
        return num;
        }

    public void printBullandCow(int playerNum, int secretCode)
        {
        int[] pnum = new int[4];
        int[] snum = new int[4];
        int bull = 0;
        int cow = 0;
        for (int i = 0; i < 4; i++) {
            pnum[i] = playerNum % 10;
            playerNum -= pnum[i];
            playerNum /= 10;
            snum[i] = secretCode % 10;
            secretCode -= snum[i];
            secretCode /= 10;
            if (pnum[i] == snum[i]) {
                bull++;
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (pnum[j] == snum[i]) {
                    cow++;
                }
            }
            if (cow == 4) {
                break;
            }
        }
        System.out.println("Result: " + bull + " bull and " + cow + " cows.");
        }

    public boolean match(int pnum, int snum)
        {
        if (pnum == snum) {
            return true;
        }
        return false;
        }

    public int[] loadFile()
        {

        int i = 0;
        while (true) {
            System.out.print("Enter the filename you want to load: ");
            String fileName = Keyboard.readInput();
            File myFile = new File(fileName);
            try (Scanner scanner1 = new Scanner(myFile)) {
                int count = 0;
                while (scanner1.hasNextLine()) {
                    count++;
                    scanner1.nextLine();
                }
                Scanner scanner = new Scanner(myFile);
                scanner.useDelimiter("\\r\\n");
                int[] a = new int[count];
                while (scanner.hasNext()) {
                    int m = scanner.nextInt();
                    a[i] = m;
                    i++;
                }
                return a;
            } catch (NullPointerException e) {
                System.out.println("Error: " + e.getMessage());
            } catch (FileNotFoundException err) {
                System.out.println("Error: Files cannot found!");
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
        }

    public static void main(String[] args) throws FileNotFoundException
        {
        mainProgram ruby = new mainProgram();
        ruby.start();
        }
    }
