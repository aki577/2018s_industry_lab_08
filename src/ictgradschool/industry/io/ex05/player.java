package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

public class player
    {
    protected String name;
    protected int guess;
    protected String result;

    public player()
        {
        guess = 0;
        name = "";
        }

    public int guess()
        {
        guess = Integer.parseInt(Keyboard.readInput());
        return guess;
        }

        public void setName(String name){
        this.name = name;
        }

    public String getName()
        {
        return name;
        }
    }
