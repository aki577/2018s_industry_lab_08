package ictgradschool.industry.io.ex01;

import java.io.*;

public class ExerciseOne
    {

    public void start()
        {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

        }

    private void printNumEsWithFileReader()
        {

        int numE = 0;
        int total = 0;
        int num;
        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        try (FileReader fr = new FileReader("input2.txt")) {
            while ((num = fr.read()) != -1) {
                System.out.print((char) num);
                if (num == 69 || num == 101) {
                    numE++;
                }
                total++;
            }
            System.out.println();
        } catch (FileNotFoundException err) {
            System.out.println("Does not found the file.");
        } catch (IOException err) {
            System.out.println("IO problem" + err);
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
        }

    private void printNumEsWithBufferedReader()
        {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File myFile = new File("input2.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(myFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                char[] l = line.toCharArray();
                for (int i = 0; i < l.length; i++) {
                    if (l[i] == 'e' || l[i] == 'E') {
                        numE++;
                    }
                    total++;
                }
            }
        } catch (IOException err) {
            System.out.println("Error: " + err.getMessage());
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
        }

    public static void main(String[] args)
        {
        new ExerciseOne().start();
        }

    }
